import org.junit.Assert;
import org.junit.Test;
import org.okalman.servlet.ServletService;

/**
 * Created by okalman on 12/29/14.
 */
public class PostTest {
    private static final String login="xkalman";
    private static final String password="Rakonbu9on";

    @Test
    public void mealsTest(){
        String response="";
        try {
            response = new ServletService().renewCache(login, password);


        }catch (Exception e){
            e.printStackTrace();
            Assert.assertTrue("Exception was caught", false);
        }

        System.out.println(response);
        Assert.assertTrue("Response is empty", response.length()>0);
    }

    @Test
    public void moneyTest(){
        String response="";
        try {
            response = new ServletService().getMoney(login, password);
        }catch (Exception e){
            e.printStackTrace();
            Assert.assertTrue("Exception was caught", false);
        }

        System.out.println(response);
        Assert.assertTrue("Response is empty", response.length()>0);
    }

    @Test
    public void credentialsTest(){
        String response="";
        try {
            response = new ServletService().checkCredentials(login, password);


        }catch (Exception e){
            e.printStackTrace();
            Assert.assertTrue("Exception was caught", false);
        }

        System.out.println(response);
        Assert.assertTrue("Response is empty", response.length()>0);

    }
}
