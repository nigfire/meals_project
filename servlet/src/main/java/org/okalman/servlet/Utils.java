package org.okalman.servlet;

/**
 * Created by okalman on 1/2/15.
 */
public class Utils {
    public static final int ERR_INVALID_CREDENTIALS=2;
    public static final int ERR_INVALID_DATA=1;
    public static final int SUCCESS=0;

    public static String getSimpleResponse(int responseCode){
        StringBuilder sb= new StringBuilder();
        sb.append("{\"responseCode\":");
        sb.append(responseCode);
        sb.append("}");
        return sb.toString();
    }

}
