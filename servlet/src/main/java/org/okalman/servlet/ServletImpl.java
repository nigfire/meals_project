package org.okalman.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by okalman on 12/29/14.
 */

@SuppressWarnings("serial")
@WebServlet("/Meals")
public class ServletImpl extends HttpServlet {
    private static String CACHE="";
    private static long TIME = 0;
    private static final long VALIDITY=10000;
    private static final long VALIDITY_LONG_TERM=3600000;
    private static Object CACHE_LOCK= new Object();
    private static Object TIME_LOCK= new Object();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }
    @Override
    protected  void doPost(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException{
        resp.setContentType("text;charset=UTF-8");
        String method="";
        if(req.getParameter("method")!=null){
            method=req.getParameter("method");
        }

        if(method.equals("meals")){
            meals(req, resp);
        }else if(method.equals("credentials") ) {
            loginCheck(req, resp);
        }else if(method.equals("money")) {
            money(req,resp);
        }else{
            uknown(req,resp);
        }
    }

    private void loginCheck(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String response="";
        PrintWriter writer = resp.getWriter();
        ServletService service = new ServletService();
        if(req.getParameter("login")==null || req.getParameter("password")==null){
            writer.println(Utils.getSimpleResponse(Utils.ERR_INVALID_CREDENTIALS));
            writer.close();
            return ;
        }
        response=service.checkCredentials(req.getParameter("login"), req.getParameter("password"));
        writer.println(response);
        writer.close();


    }

    private void money(HttpServletRequest req, HttpServletResponse resp) throws IOException{
        String response="";
        PrintWriter writer = resp.getWriter();
        ServletService service = new ServletService();
        response=service.getMoney(req.getParameter("login"), req.getParameter("password"));
        writer.println(response);
        writer.close();


    }

    private void meals(HttpServletRequest req, HttpServletResponse resp) throws IOException{

        PrintWriter writer = resp.getWriter();
        if(req.getParameter("login")==null || req.getParameter("password")==null){
            writer.println(getCACHE());
            writer.close();
            return ;
        }

        long currentTime= System.currentTimeMillis();
        if(currentTime-getTIME()>VALIDITY || CACHE.length()==0){

            try {
                class AsyncRenew implements Runnable{
                    HttpServletRequest req;
                    String response="";
                    ServletService service = new ServletService();
                    AsyncRenew(HttpServletRequest req){
                        this.req=req;
                    }
                    @Override
                    public void run(){
                        response=service.renewCache(req.getParameter("login"), req.getParameter("password"));
                        if(response.contains("\"responseCode\":"+Utils.SUCCESS)) {
                            ServletImpl.setCACHE(response);
                            ServletImpl.setTIME(System.currentTimeMillis());
                        }

                    }
                }
                Thread t = new Thread(new AsyncRenew(req));
                t.start();
                if(CACHE.length()==0||currentTime-getTIME()>VALIDITY_LONG_TERM){
                    t.join(120000);
                }

            }catch (Exception e){

            }
        }
        writer.println(getCACHE());
        writer.close();




    }
    private void uknown(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println("Unknown method");
        writer.close();

    }

    public static String getCACHE() {
        synchronized (CACHE_LOCK) {
            return CACHE;
        }
    }

    public static  void setCACHE(String CACHE) {
        synchronized (CACHE_LOCK) {
            ServletImpl.CACHE = CACHE;
        }
    }

    public static long getTIME() {
        synchronized (TIME_LOCK) {
            return TIME;
        }
    }

    public static void setTIME(long TIME) {
        synchronized (TIME_LOCK) {
            ServletImpl.TIME = TIME;
        }
    }





}
