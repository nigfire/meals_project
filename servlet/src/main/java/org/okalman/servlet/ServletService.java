package org.okalman.servlet;


import org.apache.commons.io.IOUtils;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.codehaus.jackson.map.ObjectMapper;
import org.okalman.meals.Canteen;
import org.okalman.meals.MoneyResponse;
import org.okalman.meals.ResponseCanteens;
import org.okalman.parser.HtmlParser;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 12/29/14.
 */
public class ServletService {


    private static String[] CANTEEN_NAMES={"","JAK", "MENDELU", "TAK", "", "LEDNICE", "ZF LEDNICE"};
    private static String URL_LOGIN="https://is.mendelu.cz/system/login.pl";
    private static String URL_MEALS ="https://is.mendelu.cz/auth/uskm/jidelnicek.pl?vydejna=";
    private final String USER_AGENT = "Mozilla/5.0";
    public String checkCredentials(String login, String password){
        String token=getToken(login, password);
        if(token.equals("invalid")){
            return Utils.getSimpleResponse(Utils.ERR_INVALID_CREDENTIALS);
        }else{
            return Utils.getSimpleResponse(Utils.SUCCESS);
        }


    }

    public String renewCache(String login, String password){
        ResponseCanteens response= new ResponseCanteens();
        List<Canteen>canteenList= new ArrayList<Canteen>();
        String token=getToken(login, password);
        HtmlParser parser=new HtmlParser();
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        if(token.equals("invalid")){
            return Utils.getSimpleResponse(Utils.ERR_INVALID_CREDENTIALS);
        }


        for(int i=1; i<7; i++){
            if(i==4){
                continue;
            }
            String data=getData(token,i);
            canteenList.add(parser.getCanteen(data,CANTEEN_NAMES[i]));

        }

        response.setCanteens(canteenList);
        ;
        try {
            mapper.writeValue(writer,response);
        } catch (IOException e) {
            writer.write(Utils.getSimpleResponse(Utils.ERR_INVALID_DATA));
        }

        return writer.toString();
    }

    public String getMoney(String login, String password){
        String token=getToken(login, password);
        HtmlParser parser=new HtmlParser();
        ObjectMapper mapper = new ObjectMapper();
        StringWriter writer = new StringWriter();
        if(token.equals("invalid")){
            return Utils.getSimpleResponse(Utils.ERR_INVALID_CREDENTIALS);
        }
        String data=getData(token,1);
        MoneyResponse money = parser.getMoney(data);
        try {
            mapper.writeValue(writer,money);
        }catch(Exception e){
            writer.write(Utils.getSimpleResponse(Utils.ERR_INVALID_DATA));
        }
        return writer.toString();
    }



    private String getToken(String login,String password){
        String token=null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(URL_LOGIN);
            httppost.setHeader("Accept-Language","en");
            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("credential_0", login));
            params.add(new BasicNameValuePair("credential_1", password));
            params.add(new BasicNameValuePair("credential_2", "86400"));
            params.add(new BasicNameValuePair("login", "Přihlásitse"));
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            HeaderElement[] elements = response.getFirstHeader("Set-Cookie").getElements();
            for (int i = 0; i < elements.length; i++) {
                if (elements[i].getName().equals("UISAuth")) {
                    token = elements[i].getValue();
                }
            }
        }catch(Exception e){
            token="invalid";
        }

        return token;
    }

    private String getData(String token, int canteenId){

        HttpResponse response=null;
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(URL_MEALS +canteenId);
            HttpParams params= new BasicHttpParams();
            httpget.setHeader("Cookie","UISAuth="+token);
            httpget.setHeader("Accept-Language","en");

            httpget.setParams(params);
            response = httpclient.execute(httpget);


        }catch(Exception e){
            token="invalid";
        }
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(response.getEntity().getContent(), writer, "iso-8859-2");
        } catch (IOException e) {
            e.printStackTrace();
        }


        return writer.toString();
    }

}
