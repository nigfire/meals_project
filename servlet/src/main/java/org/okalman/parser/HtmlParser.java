package org.okalman.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.okalman.meals.Canteen;
import org.okalman.meals.Meal;
import org.okalman.meals.MealDay;
import org.okalman.meals.MoneyResponse;
import org.okalman.servlet.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by okalman on 12/31/14.
 */
public class HtmlParser {
//Account balance 79,50 CZK

    private static final String DATE_REGEXP= "((Po|Út|St|Čt|Pá)\\s[0-3][0-9]\\.\\s[0-1][0-9]\\.\\s[0-9]{4})";
    private static final String MONEY_REGEXP= "([0-9]+,[0-9]*)&nbsp;CZK";
    private static int tablesOffset=-1;


    public MoneyResponse getMoney(String htmlPage){
        Pattern p= Pattern.compile(MONEY_REGEXP);
        Matcher m= p.matcher(htmlPage);
        MoneyResponse match = new MoneyResponse();
        match.setResponseCode(Utils.ERR_INVALID_DATA);
        while (m.find()){
            match.setMoney(m.group(1));
            match.setResponseCode(Utils.SUCCESS);
        }
        return match;
    }

    public Canteen getCanteen(String htmlPage, String canteenName){
         Canteen canteen = new Canteen();
        canteen.setCanteen(canteenName);
        try {
            List<String> dates= getDates(htmlPage);
            List<MealDay>mealDays= new ArrayList<MealDay>();

            for(int i=0;i<dates.size(); i++){
                MealDay day= new MealDay();
                day.setDate(dates.get(i).substring(3).replaceAll("\\s",""));
                day.setDay(dates.get(i).substring(0,2).replaceAll("\\s",""));
                day.setMeals(getMealsForDay(i, htmlPage));
                mealDays.add(day);
            }
            canteen.setMealDays(mealDays);

        }catch(Exception e){
            e.printStackTrace();
        }


        return canteen;
    }

    public List<String> getDates(String htmlPage){
        Pattern p= Pattern.compile(DATE_REGEXP);
        Matcher m= p.matcher(htmlPage);
        ArrayList<String> matches = new ArrayList<String>();
        while (m.find()){
            matches.add(m.group());
        }
        return matches;
    }
    public List<Meal> getMealsForDay(int dayIndex, String htmlPage){
        ArrayList<Meal>meals=new ArrayList<Meal>();
        Document doc =Jsoup.parse(htmlPage);
        try {
            Element table = doc.select("table").get(dayIndex + getOffset(htmlPage));
            Elements rows = table.select("tr");

            for (int i = 1; i < rows.size(); i++) { //first row is the col names so skip it.
                Element row = rows.get(i);
                Elements cols = row.select("td");
                Element mealType = cols.get(0).select("small").get(0);
                Element mealName = cols.get(2).select("small").get(0);
                Meal meal = new Meal();
                meal.setMeal(mealName.text());
                meal.setType(mealType.text());
                meals.add(meal);

            }

        }catch(Exception e){

        }


        return meals;
    }

    private int getOffset(String htmlPage){
        if(tablesOffset>-1){
            return tablesOffset;
        }else{
            Document doc =Jsoup.parse(htmlPage);
            for(int i=0; i<doc.select("table").size(); i++){
                Element table = doc.select("table").get(i);

                if(table.html().contains("Kind of meal")||table.html().contains("Druh jídla")){
                    tablesOffset=i;
                    return i;
                }
            }

            return tablesOffset;

        }

    }



}
