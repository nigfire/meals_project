package org.okalman.meals;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.okalman.servlet.Utils;

/**
 * Created by okalman on 1/5/15.
 */

@JsonAutoDetect
public class MoneyResponse {

    private String money= new String();
    private int responseCode= Utils.SUCCESS;
    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }


}
