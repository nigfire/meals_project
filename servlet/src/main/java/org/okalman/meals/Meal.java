package org.okalman.meals;

import org.codehaus.jackson.annotate.JsonAutoDetect;

/**
 * Created by okalman on 12/31/14.
 */
@JsonAutoDetect
public class Meal {

    private String meal="";
    private String type="";

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
