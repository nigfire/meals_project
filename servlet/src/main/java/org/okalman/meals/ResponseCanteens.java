package org.okalman.meals;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.okalman.servlet.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 1/1/15.
 */
@JsonAutoDetect
public class ResponseCanteens {

    private List<Canteen> canteens= new ArrayList<Canteen>();
    private int responseCode= Utils.SUCCESS;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public List<Canteen> getCanteens() {
        return canteens;
    }

    public void setCanteens(List<Canteen> canteens) {
        this.canteens = canteens;
    }

}
