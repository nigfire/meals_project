package org.okalman.meals;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 12/31/14.
 */

@JsonAutoDetect
public class MealDay {

    private String date;
    private String day;
    private List<Meal> meals = new ArrayList<Meal>();


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }



}
