package org.okalman.meals;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 12/31/14.
 */
@JsonAutoDetect
public class Canteen {
    private String canteen="";
    private List<MealDay> mealDays= new ArrayList<MealDay>();

    public String getCanteen() {
        return canteen;
    }

    public void setCanteen(String canteen) {
        this.canteen = canteen;
    }

    public List<MealDay> getMealDays() {
        return mealDays;
    }

    public void setMealDays(List<MealDay> mealDays) {
        this.mealDays = mealDays;
    }


}
