package com.meals.okalman.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by okalman on 1/2/15.
 */
public class MyApp extends Application {
    private static MyApp instance = null;

    public MyApp() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }


}
