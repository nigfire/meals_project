package com.meals.okalman.tools;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.os.Bundle;

/**
 * Created by okalman on 1/2/15.
 */
public class ProgressFragmentPopUp extends DialogFragment {




    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        ProgressDialog dialog = new ProgressDialog(getActivity(), getTheme());
        dialog.setTitle(this.getArguments().getString("title"));
        dialog.setMessage(this.getArguments().getString("message"));
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return dialog;
    }

    public static ProgressFragmentPopUp newInstance(String title, String message){
        ProgressFragmentPopUp p= new ProgressFragmentPopUp();
        Bundle args= new Bundle();
        args.putString("title",title);
        args.putString("message",message);
        p.setArguments(args);
        return p;

    }
}
