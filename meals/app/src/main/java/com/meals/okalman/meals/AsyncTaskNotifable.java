package com.meals.okalman.meals;

/**
 * Created by okalman on 1/2/15.
 */
public interface AsyncTaskNotifable<T> {

    void onPreExecute(Class notifier, String info, Object data);
    void onProgressUpdate(Class notifier, String info, Object data);
    void onPostExecute(Class notifier, String info, Object data);

}
