package com.meals.okalman.datamodel;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 1/2/15.
 */
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Canteen {
    private String canteen="";
    private List<MealDay> mealDays= new ArrayList<MealDay>();

    public String getCanteen() {
        return canteen;
    }

    public void setCanteen(String canteen) {
        this.canteen = canteen;
    }

    public List<MealDay> getMealDays() {
        return mealDays;
    }

    public void setMealDays(List<MealDay> mealDays) {
        this.mealDays = mealDays;
    }


}
