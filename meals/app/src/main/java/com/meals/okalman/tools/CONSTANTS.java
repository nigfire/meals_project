package com.meals.okalman.tools;

/**
 * Created by okalman on 1/2/15.
 */
public class CONSTANTS {
    public static final String EXTRAS_KEY_DATE="date";
    public static final String EXTRAS_KEY_CANTEEN="canteen";

    public static final int ERR_INVALID_CREDENTIALS=2;
    public static final int ERR_INVALID_DATA=1;
    public static final int SUCCESS=0;
    public static final String CONST_PREF_KEY="com.meals.okalman.prefs";

    public static final String HTTP_METHOD_MEALS="meals";
    public static final String HTTP_METHOD_CREDENTIALS="credentials";

    public static final String DB_METHOD_GET_CANTEEN_NAMES="getCanteenNames";
    public static final String DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS="getActualDaysEmptyMeals";
    public static final String DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY="getMealsforCanteenAndDay";


}

