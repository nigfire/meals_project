package com.meals.okalman.database;

import android.os.AsyncTask;

import com.meals.okalman.meals.AsyncTaskNotifable;
import com.meals.okalman.tools.CONSTANTS;

/**
 * Created by okalman on 1/3/15.
 */
public class DbGetDataAsyncTask extends AsyncTask<String,Void,String> {

    Object data;
    AsyncTaskNotifable callback;
    public DbGetDataAsyncTask(AsyncTaskNotifable callback){
        this.callback=callback;
    }

    @Override
    protected String doInBackground(String... params) {
        DbDataSource db= new DbDataSource();
        switch (params[0]){
            case CONSTANTS.DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS:{
               data= db.getActualDaysEmptyMeals(params[1]);
               return CONSTANTS.DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS;

            }
            case CONSTANTS.DB_METHOD_GET_CANTEEN_NAMES:{
                data=db.getCanteenNames();
                return CONSTANTS.DB_METHOD_GET_CANTEEN_NAMES;

            }
            case CONSTANTS.DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY:{
                data=db.getMealsforCanteenAndDay(params[1],params[2]);
                return CONSTANTS.DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY;

            }



        }



      return "false";
    }

    @Override
    protected void onPostExecute(String s){
        callback.onPostExecute(this.getClass(), s,data);


    }
}
