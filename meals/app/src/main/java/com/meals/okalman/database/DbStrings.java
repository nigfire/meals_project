package com.meals.okalman.database;

/**
 * Created by okalman on 1/2/15.
 */
public class DbStrings {
    protected static final int DATABASE_VERSION = 3;

    protected static final String DATABASE_NAME = "meals.db";

    protected static final String TABLE_MEALS="MEALS_TABLE";
    protected static final String MEALS_COLUMN_ID="id";
    protected static final String MEALS_COLUMN_CANTEEN="canteen";
    protected static final String MEALS_COLUMN_MEAL="meal";
    protected static final String MEALS_COLUMN_MEAL_TYPE="mealType";
    protected static final String MEALS_COLUMN_DATE="date";
    protected static final String MEALS_COLUMN_DAY = "day";

    protected static final String[] MEALS_ALL_COLUMNS={MEALS_COLUMN_ID,MEALS_COLUMN_CANTEEN,MEALS_COLUMN_MEAL,MEALS_COLUMN_MEAL_TYPE,MEALS_COLUMN_DATE,MEALS_COLUMN_DAY};

    protected static final String CREATE_TABLE_MEALS="create table " + TABLE_MEALS + "("
            + MEALS_COLUMN_ID + " integer primary key autoincrement, "
            + MEALS_COLUMN_CANTEEN + " text not null, "
            + MEALS_COLUMN_MEAL + " text not null, "
            + MEALS_COLUMN_MEAL_TYPE + " text not null, "
            + MEALS_COLUMN_DATE +  " integer not null, "
            + MEALS_COLUMN_DAY +  " text not null);";

    protected static final String DROP_TABLES="drop table if exists "+ TABLE_MEALS;




}
