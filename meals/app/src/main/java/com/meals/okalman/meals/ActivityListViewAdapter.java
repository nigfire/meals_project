package com.meals.okalman.meals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.meals.okalman.datamodel.MealDay;

import java.util.List;

/**
 * Created by okalman on 1/3/15.
 */
public class ActivityListViewAdapter extends BaseAdapter {
    Context context;
    List<MealDay>mealDays;


    ActivityListViewAdapter(Context context, List<MealDay>mealDays){
        this.context=context;
        this.mealDays=mealDays;

    }
    @Override
    public int getCount() {
        return mealDays.size();
    }

    @Override
    public Object getItem(int position) {
        return mealDays.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       MyHolder holder;
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.main_list_item,parent,false);
            holder = new MyHolder();
            holder.textViewDay = (TextView) convertView.findViewById(R.id.main_list_item_day);
            holder.textViewDate = (TextView) convertView.findViewById(R.id.main_list_item_date);
            convertView.setTag(holder);
        }else{
            holder=(MyHolder)convertView.getTag();
        }
        holder.textViewDay.setText(mealDays.get(position).getDay());
        holder.textViewDate.setText(mealDays.get(position).getDate());
        convertView.setTag(holder.textViewDate.getId(),mealDays.get(position).getDate());
        return convertView;
    }

    private static class MyHolder{
        public TextView textViewDay;
        public TextView textViewDate;


    }
}
