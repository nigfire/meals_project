package com.meals.okalman.http;

/**
 * Created by okalman on 1/2/15.
 */
public class HttpConnectorException extends Exception {

    String serverResponse;
    HttpConnectorException(String message, String serverResponse){
        super(message);
        this.serverResponse=serverResponse;
    }
    public String getServerResponse() {
        return serverResponse;
    }

}
