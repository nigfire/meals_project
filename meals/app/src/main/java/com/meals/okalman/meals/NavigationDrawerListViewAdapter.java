package com.meals.okalman.meals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by okalman on 1/3/15.
 */
public class NavigationDrawerListViewAdapter extends BaseAdapter {

     Context context;
     List<String>canteens;


    public NavigationDrawerListViewAdapter(Context context, List<String> canteens) {
        this.context=context;
        this.canteens=canteens;
    }


    @Override
    public int getCount() {
        return canteens.size();
    }

    @Override
    public Object getItem(int position) {
       return canteens.get(position) ;
    }

    @Override
    public long getItemId(int position) {
       return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder holder;
        // Get the data item for this position
        if(convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.drawer_list_item,parent,false);
            holder= new MyHolder();
            holder.textview=(TextView)convertView.findViewById(R.id.drawer_list_item_canteen);
            holder.imageView=(ImageView)convertView.findViewById(R.id.drawer_list_item_icon);
            convertView.setTag(holder);

        }else{
            holder= (MyHolder) convertView.getTag();
        }
        // Lookup view for data population
        String canteen=canteens.get(position);
        holder.textview.setText(canteen);


        // Return the completed view to render on screen
        convertView.setTag(holder.textview.getId(),canteen);
        return convertView;
    }

    private static class MyHolder{
        public  TextView textview;
        public ImageView imageView;


    }


}
