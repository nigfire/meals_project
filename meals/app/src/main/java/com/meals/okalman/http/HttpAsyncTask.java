package com.meals.okalman.http;

import android.os.AsyncTask;

import com.meals.okalman.database.DbDataSource;
import com.meals.okalman.datamodel.Canteen;
import com.meals.okalman.meals.AsyncTaskNotifable;
import com.meals.okalman.tools.CONSTANTS;

import java.util.List;

/**
 * Created by okalman on 1/2/15.
 */
public class HttpAsyncTask extends AsyncTask<String, Void, String> {
    AsyncTaskNotifable callBack;

    public HttpAsyncTask(AsyncTaskNotifable cb){
        callBack=cb;
    }

    @Override
    protected String doInBackground(String... params) {
       String result;
       switch(params[0]){
           case CONSTANTS.HTTP_METHOD_MEALS:{
               try{
                  List<Canteen> canteen = HttpConnector.getCanteen();
                   if(canteen!=null){
                       result="true";
                   }else{
                       result="false";
                   }
               DbDataSource ds= new DbDataSource();
                   ds.renewCache(canteen);
               }catch(HttpConnectorException e){
                   return e.getMessage();
               }
               return result;

           }
           case CONSTANTS.HTTP_METHOD_CREDENTIALS:{

               try{
                   result=String.valueOf(HttpConnector.validCredentials(params[1],params[2]));
               }catch(HttpConnectorException e){
                   return e.getMessage();
               }
               return result;

           }
           default:return null;


       }

    }

    @Override
    protected void onPostExecute(String result){
        callBack.onPostExecute(this.getClass(), result,null);
    }
}
