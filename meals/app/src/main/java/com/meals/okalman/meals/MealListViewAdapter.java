package com.meals.okalman.meals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.meals.okalman.datamodel.Meal;

import java.util.List;

/**
 * Created by okalman on 1/3/15.
 */
public class MealListViewAdapter extends BaseAdapter {
    Context context;
    List<Meal> meals;


    MealListViewAdapter(Context context, List<Meal>meals){
        this.context=context;
        this.meals =meals;

    }
    @Override
    public int getCount() {
        return meals.size();
    }

    @Override
    public Object getItem(int position) {
        return meals.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyHolder holder;
        if(convertView==null){
            convertView= LayoutInflater.from(context).inflate(R.layout.meal_list_item,parent,false);
            holder = new MyHolder();
            holder.textViewMeal = (TextView) convertView.findViewById(R.id.meal_list_item_meal);
            holder.textViewType = (TextView) convertView.findViewById(R.id.meal_list_item_type);
            convertView.setTag(holder);
        }else{
            holder=(MyHolder)convertView.getTag();
        }
        holder.textViewMeal.setText(meals.get(position).getMeal());
        holder.textViewType.setText(meals.get(position).getType());
        return convertView;
    }

    private static class MyHolder{
        public TextView textViewMeal;
        public TextView textViewType;


    }
}
