package com.meals.okalman.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.util.Log;

import com.meals.okalman.app.MyApp;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by okalman on 1/2/15.
 */
public class Utils {



    public static String getServerUrl(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        return prefs.getString("serverUrl","https://jbossas-okalman.rhcloud.com/servlet/Meals");
    }
    public static void setServerUrl(String server){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putString("serverUrl",server).apply();
    }
    public static String getUserLogin(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        return prefs.getString("login",null);
    }
    public static void setUserLogin(String login){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putString("login",login).apply();
    }
    public static String getUserPassword(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        return prefs.getString("password",null);
    }
    public static void setUserPassword(String password){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putString("password",password).apply();
    }
    public static void setLastCanteen(String canteen){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putString("canteen",canteen).apply();
    }
    public static String getLastCanteen(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        return prefs.getString("canteen","JAK");
    }
    public static void setUserLearnedDrawer(boolean b){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("userLearnedDrawer", b).apply()  ;
    }
    public static boolean getUserLearnedDrawer(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
       return  prefs.getBoolean("userLearnedDrawer",false);
    }
    public static void setFirstRun(boolean b){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putBoolean("firstRun", b).apply()  ;
    }
    public static boolean getFirstRun(){
        SharedPreferences prefs= MyApp.getContext().getSharedPreferences(CONSTANTS.CONST_PREF_KEY, Context.MODE_PRIVATE);
        return  prefs.getBoolean("firstRun",true);
    }

    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) MyApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo()!=null){
            return true;
        }
        return false;
    }




    public static String getDbDate(String date){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        DateTime dt = formatter.parseDateTime(date);
        DateTimeFormatter dbFromatter= DateTimeFormat.forPattern("yyyyMMdd");
    return dbFromatter.print(dt);
}
    public static String getHumanDateFromDb(String date){
        DateTimeFormatter dbFromatter= DateTimeFormat.forPattern("yyyyMMdd");
        DateTime dt = dbFromatter.parseDateTime(date);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yyyy");
        return formatter.print(dt);

    }





}
