package com.meals.okalman.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.meals.okalman.datamodel.Canteen;
import com.meals.okalman.datamodel.Meal;
import com.meals.okalman.datamodel.MealDay;
import com.meals.okalman.tools.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by okalman on 1/3/15.
 */
public class DbDataSource {
    SQLiteDatabase database=null;

    private SQLiteDatabase getDatabase(){
        if(database==null){
            database= DbHelper.getInstance().getWritableDatabase();
        }
        return database;
    }

 private synchronized void fillUpDatabase(List<Canteen>canteens){
 SQLiteDatabase database= getDatabase();
     for(Canteen c: canteens){

         for(MealDay md: c.getMealDays()){

             for(Meal m: md.getMeals()){
                 ContentValues cv = new ContentValues();
                 cv.put(DbStrings.MEALS_COLUMN_CANTEEN,c.getCanteen());
                 cv.put(DbStrings.MEALS_COLUMN_DATE, Utils.getDbDate(md.getDate()));
                 Log.v(this.getClass().toString()+"fill",md.getDate());
                 cv.put(DbStrings.MEALS_COLUMN_DAY,md.getDay());
                 cv.put(DbStrings.MEALS_COLUMN_MEAL,m.getMeal());
                 cv.put(DbStrings.MEALS_COLUMN_MEAL_TYPE,m.getType());
                 database.insert(DbStrings.TABLE_MEALS, null, cv);
             }

         }

     }
 }
 public synchronized void renewCache(List<Canteen>canteens){
     if(canteens==null || canteens.size()==0){
         return;
     }
     getDatabase().beginTransaction();

     try {
         getDatabase().execSQL(DbStrings.DROP_TABLES);
         getDatabase().execSQL(DbStrings.CREATE_TABLE_MEALS);
         fillUpDatabase(canteens);
         getDatabase().setTransactionSuccessful();
     }catch (Exception e){
         e.printStackTrace();
     }finally {
         getDatabase().endTransaction();
     }
 }

 public synchronized List<MealDay> getActualDaysEmptyMeals(String canteen) {
     SQLiteDatabase database=getDatabase();
     Calendar calendar = Calendar.getInstance();
     List<MealDay>days=new ArrayList<>();
     System.out.println("Current time => " + calendar.getTime());
     SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
     String todayDate = df.format(calendar.getTime());
     Log.v(this.getClass().toString(),todayDate);
     Cursor c=database.query(DbStrings.TABLE_MEALS,new String[]{DbStrings.MEALS_COLUMN_DATE,DbStrings.MEALS_COLUMN_DAY}, DbStrings.MEALS_COLUMN_DAY+" >= ? AND "+DbStrings.MEALS_COLUMN_CANTEEN+" = ?", new String[]{todayDate,canteen},DbStrings.MEALS_COLUMN_DATE+", "+DbStrings.MEALS_COLUMN_DAY,null,null);
     while(c.moveToNext()) {
         MealDay md = new MealDay();
         md.setDay(c.getString(c.getColumnIndex(DbStrings.MEALS_COLUMN_DAY)));
         md.setDate(Utils.getHumanDateFromDb(c.getString(c.getColumnIndex(DbStrings.MEALS_COLUMN_DATE))));
         days.add(md);
     }
    return days;
 }

 public synchronized List<Meal> getMealsforCanteenAndDay(String canteen,String date){
     SQLiteDatabase database=getDatabase();
     List<Meal>meals= new ArrayList<>();
     String dbDate= Utils.getDbDate(date);
     Cursor c = database.query(DbStrings.TABLE_MEALS,DbStrings.MEALS_ALL_COLUMNS, DbStrings.MEALS_COLUMN_CANTEEN + " = ? AND "+ DbStrings.MEALS_COLUMN_DATE+" = ?",new String[]{canteen,dbDate},null,null,null);
     while(c.moveToNext()) {
         Meal m = new Meal();
         m.setMeal(c.getString(c.getColumnIndex(DbStrings.MEALS_COLUMN_MEAL)));
         m.setType(c.getString(c.getColumnIndex(DbStrings.MEALS_COLUMN_MEAL_TYPE)));
         meals.add(m);
     }

    return meals;
 }
 public synchronized List<String>getCanteenNames() {
     SQLiteDatabase database=getDatabase();
     List<String>canteens= new ArrayList<>();
     Cursor c = database.query(DbStrings.TABLE_MEALS,new String[]{DbStrings.MEALS_COLUMN_CANTEEN}, null,null,DbStrings.MEALS_COLUMN_CANTEEN,null,null);
     while(c.moveToNext()) {
         canteens.add(c.getString(0));
     }

     return canteens;

 }



}

