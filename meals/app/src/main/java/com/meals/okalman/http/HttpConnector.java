package com.meals.okalman.http;

import android.util.Log;

import com.meals.okalman.app.MyApp;
import com.meals.okalman.datamodel.Canteen;
import com.meals.okalman.datamodel.ResponseClass;
import com.meals.okalman.datamodel.SimpleResponse;
import com.meals.okalman.meals.R;
import com.meals.okalman.tools.CONSTANTS;
import com.meals.okalman.tools.Utils;

import org.apache.http.client.HttpClient;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by okalman on 1/2/15.
 */
public class HttpConnector {

    public static List<Canteen> getCanteen() throws  HttpConnectorException{

        String login= Utils.getUserLogin();
        String password=Utils.getUserPassword();
        String data=getData(login,password, CONSTANTS.HTTP_METHOD_MEALS);
        ObjectMapper mapper = new ObjectMapper();
        try {
            ResponseClass response = mapper.readValue(data, ResponseClass.class);
            switch(response.getResponseCode()){
                case CONSTANTS.ERR_INVALID_CREDENTIALS:  throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_credentials2),null);
                case CONSTANTS.ERR_INVALID_DATA:  throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_data),null);
                case CONSTANTS.SUCCESS: return response.getCanteens();
                default:throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_data),null);
            }
        }catch(IOException e){
            e.printStackTrace();
            throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_data),data);

        }



    }
    public static boolean validCredentials(String login, String password)throws HttpConnectorException{

        String data=getData(login, password, CONSTANTS.HTTP_METHOD_CREDENTIALS);
        ObjectMapper mapper = new ObjectMapper();
        try {
            SimpleResponse response = mapper.readValue(data, SimpleResponse.class);
            Log.d(HttpConnector.class.toString(),String.valueOf(response.getResponseCode()));
            switch(response.getResponseCode()){
                case CONSTANTS.ERR_INVALID_CREDENTIALS:  return false ;
                case CONSTANTS.SUCCESS: return true;
                default: throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_data),data);
            }
        }catch(IOException e){
            e.printStackTrace();
            Log.w("this.class.toString()",data);
            throw new HttpConnectorException(MyApp.getContext().getString(R.string.invalid_data),data);

        }



    }

    private static String getData(String login, String password,String method) throws HttpConnectorException{
        if(!Utils.isOnline()){
            throw new HttpConnectorException(MyApp.getContext().getString(R.string.no_internet),null);
        }

        String token=null;
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Utils.getServerUrl());
            List<NameValuePair> params = new ArrayList<NameValuePair>(2);
            params.add(new BasicNameValuePair("login", login));
            params.add(new BasicNameValuePair("password", password));
            params.add(new BasicNameValuePair("method", method));;
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            StringWriter writer = new StringWriter();
            try {
                IOUtils.copy(response.getEntity().getContent(), writer, "UTF-8");
                token=writer.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }catch(Exception e){

        }

        return token;
    }






}
