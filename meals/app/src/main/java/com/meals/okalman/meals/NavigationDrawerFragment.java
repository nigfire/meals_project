package com.meals.okalman.meals;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.meals.okalman.tools.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View container;

    private ListView listView;
    private NavigationDrawerListViewAdapter adapter;
    private List<String>listData;
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceSate){
        super.onCreate(savedInstanceSate);
        mUserLearnedDrawer=Utils.getUserLearnedDrawer();
        if(savedInstanceSate!=null){
            mFromSavedInstanceState=true;
        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        listView= (ListView)v.findViewById(R.id.drawer_list);
        listData= new ArrayList<>();
        adapter= new NavigationDrawerListViewAdapter(getActivity(),listData);
        listView.setOnItemClickListener((ListView.OnItemClickListener)getActivity());
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        return v;
    }


    public void setUp(DrawerLayout drawerLayout, final Toolbar toolbar, int containerId) {

        this.container = getActivity().findViewById(containerId);
        mDrawerLayout=drawerLayout;
        mDrawerToggle= new ActionBarDrawerToggle(getActivity(),drawerLayout,toolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close){

        @Override
        public void onDrawerOpened(View drawerView){
              super.onDrawerOpened(drawerView);
                if(!mUserLearnedDrawer){
                    mUserLearnedDrawer=true;
                    Utils.setUserLearnedDrawer(mUserLearnedDrawer);

                }
              getActivity().invalidateOptionsMenu();
            }

        @Override
        public void onDrawerClosed(View drawerView){
            super.onDrawerClosed(drawerView);
            getActivity().invalidateOptionsMenu();
        }



        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
        if(!mUserLearnedDrawer && !mFromSavedInstanceState){
            show();
        }

    }

    public void renewData(List<String>newData){
        if(newData==null || newData.size()==0){
            Log.w(this.getClass().toString(),"Probbably invalid data from DB");
            return;
        }

        listData.clear();
        listData.addAll(newData);
        adapter.notifyDataSetChanged();

    }
    public void show(){
        mDrawerLayout.openDrawer(container);
    }

    public void hide(){
        mDrawerLayout.closeDrawer(container);

    }

}
