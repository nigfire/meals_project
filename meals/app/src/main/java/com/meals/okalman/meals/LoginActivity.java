package com.meals.okalman.meals;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.meals.okalman.http.HttpAsyncTask;
import com.meals.okalman.tools.CONSTANTS;
import com.meals.okalman.tools.ProgressFragmentPopUp;
import com.meals.okalman.tools.Utils;


public class LoginActivity extends ActionBarActivity implements AsyncTaskNotifable {
    private Toolbar toolbar;
    private EditText editTextLogin,editTextPassword;
    private ProgressFragmentPopUp progressPopUp;
    private CheckBox checkAgreed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar= (Toolbar)findViewById(R.id.app_bar);
        toolbar.setTitle(getString(R.string.login_title));
        setSupportActionBar(toolbar);
        editTextLogin = (EditText)findViewById(R.id.text_login);
        editTextPassword = (EditText)findViewById(R.id.text_password);
        checkAgreed = (CheckBox)findViewById(R.id.checkbox_agree);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnSubmit(View v) {
        if(checkAgreed.isChecked()) {
            HttpAsyncTask asyncTask = new HttpAsyncTask(this);
            asyncTask.execute(new String[]{CONSTANTS.HTTP_METHOD_CREDENTIALS, editTextLogin.getText().toString(), editTextPassword.getText().toString()});
            progressPopUp = ProgressFragmentPopUp.newInstance("", getString(R.string.credential_verification));
            progressPopUp.show(getFragmentManager(), null);
        }else{
            Toast.makeText(this,R.string.must_agree, Toast.LENGTH_LONG).show();
        }
    }

    public void showTerms(View v){
        Intent intent = new Intent(this, TermsActivity.class);
        startActivity(intent);

    }

    @Override
    public void onPreExecute(Class notifier, String info, Object data) {

    }

    @Override
    public void onProgressUpdate(Class notifier, String info, Object data) {

    }

    @Override
    public void onPostExecute(Class notifier, String info, Object data) {
        if(notifier == HttpAsyncTask.class){

            switch (info){
                case "true":{
                    Utils.setUserLogin(editTextLogin.getText().toString());
                    Utils.setUserPassword(editTextPassword.getText().toString());
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                     break;
                }
                case "false":{
                    Toast.makeText(this,getString(R.string.invalid_credentials1), Toast.LENGTH_LONG).show();
                    break;
                }
                default:{
                   // Toast.makeText(this,info, Toast.LENGTH_LONG).show();
                    break;
                }
            }
        }
        progressPopUp.dismiss();
    }
}
