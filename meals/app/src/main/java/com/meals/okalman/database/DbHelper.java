package com.meals.okalman.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.meals.okalman.app.MyApp;

/**
 * Created by okalman on 1/2/15.
 */
public class DbHelper extends SQLiteOpenHelper{

    private static DbHelper instance=null;

    private DbHelper(Context context){
        super(context, DbStrings.DATABASE_NAME, null, DbStrings.DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbStrings.CREATE_TABLE_MEALS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DbStrings.DROP_TABLES);
        db.execSQL(DbStrings.CREATE_TABLE_MEALS);
    }

    public static DbHelper getInstance(){
        if(instance==null){
            instance= new DbHelper(MyApp.getContext());
        }
        return instance;
    }


}
