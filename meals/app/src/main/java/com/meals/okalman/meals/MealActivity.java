package com.meals.okalman.meals;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.meals.okalman.database.DbGetDataAsyncTask;
import com.meals.okalman.datamodel.Meal;
import com.meals.okalman.tools.CONSTANTS;

import java.util.ArrayList;
import java.util.List;


public class MealActivity extends ActionBarActivity implements AsyncTaskNotifable {
    private String date;
    private String canteen;
    private List<Meal>meals;
    private MealListViewAdapter adapter;
    private ListView listview;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        date=getIntent().getStringExtra(CONSTANTS.EXTRAS_KEY_DATE);
        canteen=getIntent().getStringExtra(CONSTANTS.EXTRAS_KEY_CANTEEN);
        setContentView(R.layout.activity_meal);
        meals= new ArrayList<>();
        listview= (ListView)findViewById(R.id.meal_list);
        adapter = new MealListViewAdapter(this, meals);
        listview.setAdapter(adapter);
        toolbar= (Toolbar)findViewById(R.id.app_bar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_action_back);


        toolbar.setTitle("");

        if(date!=null && canteen!=null){
            toolbar.setTitle(date);
            toolbar.setSubtitle(canteen);



            DbGetDataAsyncTask asyncTask= new DbGetDataAsyncTask(this);
            //asyncTask.execute(CONSTANTS.DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY,canteen,date);
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,CONSTANTS.DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY,canteen,date);
        }








    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.d(this.getClass().toString(), "called");
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == android.R.id.home || id== R.id.home){
            NavUtils.navigateUpFromSameTask(this);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPreExecute(Class notifier, String info, Object data) {

    }

    @Override
    public void onProgressUpdate(Class notifier, String info, Object data) {

    }

    @Override
    public void onPostExecute(Class notifier, String info, Object data) {
        if(info!=null || info.equals(CONSTANTS.DB_METHOD_GET_MEALS_FOR_CANTEEN_AND_DAY)){
           try{
               ArrayList<Meal>ml= (ArrayList<Meal>)data;
               if(ml!=null && ml.size()!=0){
                  // meals.clear();
                   meals.addAll(ml);
                   adapter.notifyDataSetChanged();

               }

           }catch (Exception e){

           }

        }


    }


}
