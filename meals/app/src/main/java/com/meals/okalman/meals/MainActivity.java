package com.meals.okalman.meals;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.meals.okalman.database.DbGetDataAsyncTask;
import com.meals.okalman.datamodel.MealDay;
import com.meals.okalman.http.HttpAsyncTask;
import com.meals.okalman.tools.CONSTANTS;
import com.meals.okalman.tools.ProgressFragmentPopUp;
import com.meals.okalman.tools.Utils;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity implements ListView.OnItemClickListener, AsyncTaskNotifable {
    private Toolbar toolbar;
    private NavigationDrawerFragment drawerFragment;
    private ListView mainListView;
    private ActivityListViewAdapter mainListViewAdapter;
    private List<MealDay>dayList;
    private boolean firstRun;
    private ProgressFragmentPopUp progressPopUp;
    private ProgressBar progressBarRefesh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(Utils.getUserLogin()==null|| Utils.getUserPassword()==null){
            startActivity(new Intent(this,LoginActivity.class));
            finish();
            return;
        }
        firstRun=Utils.getFirstRun();
        if(firstRun){
            getFreshDataFromWeb();
            progressPopUp = ProgressFragmentPopUp.newInstance("", getString(R.string.initialization));
            progressPopUp.show(getFragmentManager(),null);

        }else{
            reloadAllFromDb();
        }


        toolbar= (Toolbar)findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        drawerFragment.setUp((DrawerLayout)findViewById(R.id.drawerLayout),toolbar, R.id.navigation_drawer_fragment);
        progressBarRefesh= (ProgressBar)findViewById(R.id.progressBarrefresh);
        dayList = new ArrayList<>();
        mainListView = (ListView)findViewById(R.id.main_list);
        mainListViewAdapter= new ActivityListViewAdapter(this,dayList);
        mainListView.setAdapter(mainListViewAdapter);
        mainListView.setOnItemClickListener(this);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if(id == R.id.action_refresh){

            getFreshDataFromWeb();
        }
        if(id== R.id.action_logout){
          Utils.setUserLogin(null);
          Utils.setUserPassword(null);
          Intent intent = new Intent(this, MainActivity.class);
          startActivity(intent);
          finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(view.getTag(R.id.drawer_list_item_canteen)!=null) {

            try {
                String s = (String) view.getTag(R.id.drawer_list_item_canteen);
                Utils.setLastCanteen(s);
                DbGetDataAsyncTask asyncTask2= new DbGetDataAsyncTask(this);
                asyncTask2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,CONSTANTS.DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS, Utils.getLastCanteen());


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(view.getTag(R.id.main_list_item_date)!=null){


            Intent intent= new Intent(this, MealActivity.class);
            intent.putExtra(CONSTANTS.EXTRAS_KEY_CANTEEN, Utils.getLastCanteen());
            intent.putExtra(CONSTANTS.EXTRAS_KEY_DATE, (String) view.getTag(R.id.main_list_item_date));
            startActivity(intent);
        }



    }

    @Override
    public void onPreExecute(Class notifier, String info, Object data) {

    }

    @Override
    public void onProgressUpdate(Class notifier, String info, Object data) {

    }

    @Override
    public void onPostExecute(Class notifier, String info, Object data) {
        if(notifier == HttpAsyncTask.class){
            if(info.equals("true")){
                Utils.setFirstRun(false);
                reloadAllFromDb();
            }else{

            }
            if(progressPopUp!=null){
                progressPopUp.dismiss();
            }
            if(progressBarRefesh!=null) {
                progressBarRefesh.setVisibility(View.GONE);
            }

        }else if(notifier == DbGetDataAsyncTask.class){
           if(info.equals(CONSTANTS.DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS)){
               try{
                   List<MealDay>days=(List<MealDay>)data;
                   refreshDays(days);

               }catch (Exception e){

               }

           }else if(info.equals(CONSTANTS.DB_METHOD_GET_CANTEEN_NAMES)){
               try{
                   List<String>canteens=(List<String>)data;
                   drawerFragment.renewData(canteens);

               }catch (Exception e){

               }

           }


        }
    }

    private void refreshDays(List<MealDay> dayList) {
        if(dayList==null || dayList.size()==0){
            Log.w(this.getClass().toString(), "Probbably invalid data from DB");
            return;
        }
        this.dayList.clear();
        this.dayList.addAll(dayList);
        mainListViewAdapter.notifyDataSetChanged();
        toolbar.setTitle(Utils.getLastCanteen());
        drawerFragment.hide();


    }

    private void reloadAllFromDb(){
        DbGetDataAsyncTask asyncTask1= new DbGetDataAsyncTask(this);
        asyncTask1.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, CONSTANTS.DB_METHOD_GET_CANTEEN_NAMES);
        DbGetDataAsyncTask asyncTask2= new DbGetDataAsyncTask(this);
        asyncTask2.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,CONSTANTS.DB_METHOD_GET_ACTUAL_DAYES_EMPTY_MEALS, Utils.getLastCanteen());
    }

    private void getFreshDataFromWeb(){
        if(progressBarRefesh!=null){
            progressBarRefesh.setVisibility(View.VISIBLE);
        }
        HttpAsyncTask getMeals = new HttpAsyncTask(this);
        getMeals.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,CONSTANTS.HTTP_METHOD_MEALS);
    }

}
